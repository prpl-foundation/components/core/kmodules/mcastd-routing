# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.5 - 2024-05-23(08:51:22 +0000)

### Other

- Remove KBUILD_EXTRA_SYMBOLS workaround

## Release v0.1.4 - 2024-05-06(07:11:38 +0000)

### Other

- Data model parameters needed for IGMPs in Routing Page

## Release v0.1.3 - 2024-04-24(15:35:32 +0000)

### Other

- Fixed wrong kernel module name in init script

## Release v0.1.2 - 2024-04-23(14:26:09 +0000)

### Other

- Port multicast proxy plugin (KMCD) to amx

## Release v0.1.1 - 2023-12-05(11:42:43 +0000)

### Other

- Rename mcastd plugin to tr181-mcastd

## Release v0.1.0 - 2023-12-05(11:09:46 +0000)

### New

- port multicast proxy plugin to amx

### Other

- Opensource component
- Switch license to BSD-2-Clause-Patent
- [CI] Disable cross compilation

