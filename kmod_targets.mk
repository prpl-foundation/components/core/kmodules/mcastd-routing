-include $(STAGINGDIR)/components.config
INSTALL ?= $(shell which install)

ifneq ($(KERNELRELEASE),)
# kbuild section

include $(M)/Kbuild.mk

else
include $(COMPONENT_TOPDIR)/kmod.mk
# make section
 
# compilation and linking flags
EXTRA_CFLAGS += -g -Werror

all:
	$(call Module/Compile)
ifneq ($(wildcard $(STAGINGDIR)),)
	$(call Module/Install,$(KMOD_INSTALL_PATH_PREFIX),$(KMOD_NAME))
endif

install:
	$(call Module/Install,$(KMOD_INSTALL_PATH_PREFIX),$(KMOD_NAME))

release:
	$(call Module/Release,$(KMOD_RELEASE_PATH_PREFIX),$(KMOD_NAME))

clean:
	$(call Module/Clean)

.PHONY: all clean
endif
