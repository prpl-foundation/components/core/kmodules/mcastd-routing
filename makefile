-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/mcastd-routing
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = mcastd-routing
export COMPONENT_TOPDIR = $(CURDIR)

compile:
	$(MAKE) -C $(LINUX_DIR) modules M=$(BUILD_DIR)/src

clean:
	$(MAKE) -C src clean

install:
	$(INSTALL) -D -p -m 0644 odl/mrt.odl $(D)/etc/amx/tr181-mcastd/extensions/mrt.odl
	$(INSTALL) -D -p -m 0755 scripts/S10mrt $(D)/etc/amx/tr181-mcastd/init.d/S10mrt
	$(INSTALL) -d $(D)/etc/amx/tr181-mcastd/debuginfo
	ln -sfr $(D)/etc/amx/tr181-mcastd/init.d/S10mrt $(D)/etc/amx/tr181-mcastd/debuginfo/S10mrt

.PHONY: compile clean install

