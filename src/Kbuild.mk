obj-m := $(KMOD_NAME).o
$(KMOD_NAME)-y := kmod_$(KMOD_NAME).o

ifeq ($(CONFIG_MCASTD_ROUTING_DEBUG), y)
ccflags-y += -DCONFIG_MCASTD_CORE_DEBUG
endif