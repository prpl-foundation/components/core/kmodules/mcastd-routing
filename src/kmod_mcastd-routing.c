/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/mroute.h>
#include <linux/mroute6.h>
#include <linux/icmpv6.h>
#include <linux/net.h>
#include <net/ip.h>
#include <net/ipv6.h>

#define IDENT "mrt"
#include <mcastd/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dries De Winter <dries.dewinter@softathome.com>");
MODULE_DESCRIPTION("mcastd mrt module to manage linux's multicast forwading cache entries");
MODULE_VERSION("v1.0");

enum mcastd_mrt_cmd {
	MCASTD_MRT_CMD_INIT,
	MCASTD_MRT_CMD_DONE,
	MCASTD_MRT_CMD_ADD_VIF,
	MCASTD_MRT_CMD_DEL_VIF,
	MCASTD_MRT_CMD_ADD_MFC,
	MCASTD_MRT_CMD_DEL_MFC,
};

struct mcastd_mrt_vifctl {
	int vif;
	int ifindex;
	struct mdb_address addr;
};

struct mcastd_mrt_mfcctl {
	int iif;
	struct mdb_address src;
	struct mdb_address grp;
	__u8 oif[MAXVIFS];
};

struct mcastd_mrt_msg {
	int iif;
	struct mdb_address src;
	struct mdb_address dst;
};

struct mcastd_mrt_family {
	struct list_head mfc;
	struct mdb_iface *vifs[MAXVIFS];
	struct socket *sock;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0))
	wait_queue_entry_t wait;
#else
	wait_queue_t wait;
#endif
	int (*init)(struct mcastd_mrt_family *family);
	int (*done)(struct mcastd_mrt_family *family);
	int (*vifctl)(struct mcastd_mrt_family *family,
	              struct mcastd_mrt_vifctl *ctl, int cmd);
	int (*mfcctl)(struct mcastd_mrt_family *family,
	              struct mcastd_mrt_mfcctl *ctl, int cmd);
	int (*recv)(struct mcastd_mrt_family *family, struct mcastd_mrt_msg *msg);
};

struct mcastd_mrt_iface {
	int iifenable;
	int oifenable;
	int ifindex;
	struct mdb_address addr;
	int vif;
	__u32 deldelay;
};

struct mcastd_mrt_mfc {
	struct list_head head;
	struct mcastd_mrt_family *family;
	struct mcastd_mrt_mfcctl ctl;
	struct mcastd_timer timer;
};

static int mcastd_mrt4_init(struct mcastd_mrt_family *family)
{
	int one = 1, ret;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0))
	ret = sock_create_kern(&init_net, AF_INET, SOCK_RAW, IPPROTO_IGMP, &family->sock);
#else
	ret = sock_create_kern(AF_INET, SOCK_RAW, IPPROTO_IGMP, &family->sock);
#endif
	if (ret)
		return ret;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ip_setsockopt(family->sock->sk, SOL_IP, MRT_INIT, KERNEL_SOCKPTR(&one),
	              sizeof(one));
#else
	kernel_setsockopt(family->sock, SOL_IP, MRT_INIT, (char *)&one,
	                  sizeof(one));
#endif
	return 0;
}

static int mcastd_mrt4_done(struct mcastd_mrt_family *family)
{
	int one = 1;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ip_setsockopt(family->sock->sk, SOL_IP, MRT_DONE, KERNEL_SOCKPTR(&one),
	              sizeof(one));
#else
	kernel_setsockopt(family->sock, SOL_IP, MRT_DONE, (char *)&one,
	                  sizeof(one));
#endif
	sock_release(family->sock);
	family->sock = NULL;
	return 0;
}

static int mcastd_mrt4_vifctl(struct mcastd_mrt_family *family,
                              struct mcastd_mrt_vifctl *ctl, int cmd)
{
	struct vifctl vifctl;
	int ret;
	memset(&vifctl, 0, sizeof(vifctl));
	vifctl.vifc_vifi = ctl->vif;
	vifctl.vifc_lcl_addr.s_addr = ctl->addr.data32[0];
	vifctl.vifc_rmt_addr.s_addr = htonl(INADDR_ANY);

#ifdef VIFF_USE_IFINDEX
	vifctl.vifc_flags = VIFF_USE_IFINDEX;
	vifctl.vifc_lcl_ifindex = ctl->ifindex;
#else
	vifctl.vifc_lcl_addr.s_addr = ctl->addr.data32[0];
#endif
	DEBMSG("setsockopt(cmd=0x%x vifi=%d addr=%s)",
	       MRT_BASE + cmd, ctl->vif, mdb_address_to_string(&ctl->addr));
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ret = ip_setsockopt(family->sock->sk, SOL_IP, MRT_BASE + cmd,
	                        KERNEL_SOCKPTR(&vifctl), sizeof(vifctl));
#else
	ret = kernel_setsockopt(family->sock, SOL_IP, MRT_BASE + cmd,
	                        (char *)&vifctl, sizeof(vifctl));
#endif
	if (ret) {
		ERRMSG("setsockopt(cmd=0x%x vifi=%d addr=%s) failed: %d",
		       MRT_BASE + cmd, ctl->vif,
		       mdb_address_to_string(&ctl->addr), ret);
	}
	return 0;
}

static int mcastd_mrt4_mfcctl(struct mcastd_mrt_family *family,
                              struct mcastd_mrt_mfcctl *ctl, int cmd)
{
	struct mfcctl mfcctl;
	int ret;
	int i;
	if (!family->sock) //sock already closed -> give up
		return -1;
	memset(&mfcctl, 0, sizeof(mfcctl));
	mfcctl.mfcc_parent = ctl->iif;
	mfcctl.mfcc_origin.s_addr = ctl->src.data32[0];
	mfcctl.mfcc_mcastgrp.s_addr = ctl->grp.data32[0];
	DEBMSG("setsockopt(cmd=0x%x iif=%d grp=%s src=%s", MRT_BASE + cmd,
	       ctl->iif, mdb_address_to_string(&ctl->grp),
	       mdb_address_to_string(&ctl->src));
	for (i = 0; i < MAXVIFS; i++) {
		if (!ctl->oif[i])
			continue;
		mfcctl.mfcc_ttls[i] = 1;
		DEBMSG("           oif=%d", i);
	}
	DEBMSG("           )");
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ret = ip_setsockopt(family->sock->sk, SOL_IP, MRT_BASE + cmd,
	                    KERNEL_SOCKPTR(&mfcctl), sizeof(mfcctl));
#else
	ret = kernel_setsockopt(family->sock, SOL_IP, MRT_BASE + cmd,
	                        (char *)&mfcctl, sizeof(mfcctl));
#endif
	if (ret) {
		ERRMSG("setsockopt(cmd=0x%x iif=%d grp=%s src=%s) failed: %d",
		       MRT_BASE + cmd, ctl->iif,
		       mdb_address_to_string(&ctl->grp),
		       mdb_address_to_string(&ctl->src), ret);
	}
	return ret;
}

static int mcastd_mrt4_recv(struct mcastd_mrt_family *family,
                            struct mcastd_mrt_msg *msg)
{
	int ret = -1;
	struct msghdr msghdr;
	struct kvec vec;
	struct igmpmsg igmpmsg;
	memset(&msghdr, 0, sizeof(msghdr));
	vec.iov_base = &igmpmsg;
	vec.iov_len = sizeof(igmpmsg);
	if (!family->sock) {
		DEBMSG("mrt4 sock is alread closed -> don't try to read"
		       " from it");
		return -1;
	}
	ret = kernel_recvmsg(family->sock, &msghdr, &vec, 1, vec.iov_len,
	                     MSG_DONTWAIT);
	if (ret < 0) {
		DEBMSG("kernel_recvmsg() returned error: %d", ret);
		return -1;
	} else if (!ret) {
		DEBMSG("Checked mrt4 sock, but no message available");
		return -1;
	} else if (ret < sizeof(struct igmpmsg)) {
		DEBMSG("Ignoring short message (ret=%d)", ret);
		return -1;
	}
	if (igmpmsg.im_msgtype != IGMPMSG_NOCACHE) {
		DEBMSG("Ignoring igmpmsg with msgtype=%d", igmpmsg.im_msgtype);
		return -1;
	}
	if (!igmpmsg.im_vif || igmpmsg.im_vif > MAXVIFS) {
		DEBMSG("Ignoring igmpmsg with vif=%d", igmpmsg.im_vif);
		return -1;
	}
	msg->iif = igmpmsg.im_vif;
	msg->src.family = msg->dst.family = AF_INET;
	msg->src.data32[0] = igmpmsg.im_src.s_addr;
	msg->dst.data32[0] = igmpmsg.im_dst.s_addr;
	return 0;
}

static int mcastd_mrt6_init(struct mcastd_mrt_family *family)
{
	int one = 1, ret;
	struct icmp6_filter fltr;
	memset(&fltr, 0xff, sizeof(fltr));
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0))
	ret = sock_create_kern(&init_net, AF_INET6, SOCK_RAW, IPPROTO_ICMPV6, &family->sock);
#else
	ret = sock_create_kern(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6, &family->sock);
#endif
	if (ret) {
	    ERRMSG("ret=%d", ret);
		return ret;
    }
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ipv6_setsockopt(family->sock->sk, SOL_IPV6, MRT6_INIT, KERNEL_SOCKPTR(&one),
	                sizeof(one));
	ipv6_setsockopt(family->sock->sk, SOL_IPV6, ICMPV6_FILTER,
	                KERNEL_SOCKPTR(&fltr), sizeof(fltr));
#else
	kernel_setsockopt(family->sock, SOL_IPV6, MRT6_INIT, (char *)&one,
	                  sizeof(one));
	kernel_setsockopt(family->sock, SOL_ICMPV6, ICMPV6_FILTER,
	                  (char *)&fltr, sizeof(fltr));
#endif
	return 0;
}

static int mcastd_mrt6_done(struct mcastd_mrt_family *family)
{
	int one = 1;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ipv6_setsockopt(family->sock->sk, SOL_IPV6, MRT6_DONE, KERNEL_SOCKPTR(&one),
	                sizeof(one));
#else
	kernel_setsockopt(family->sock, SOL_IPV6, MRT6_DONE, (char *)&one,
	                  sizeof(one));
#endif
	sock_release(family->sock);
	family->sock = NULL;
	return 0;
}

static int mcastd_mrt6_vifctl(struct mcastd_mrt_family *family,
                              struct mcastd_mrt_vifctl *ctl, int cmd)
{
	struct mif6ctl mif6ctl;
	int ret;
	memset(&mif6ctl, 0, sizeof(mif6ctl));
	mif6ctl.mif6c_mifi = ctl->vif;
	mif6ctl.mif6c_pifi = ctl->ifindex;
	DEBMSG("setsockopt(cmd=0x%x mifi=%d ifindex=%d)",
	       MRT6_BASE + cmd, ctl->vif, ctl->ifindex);
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ret = ipv6_setsockopt(family->sock->sk, SOL_IPV6, MRT6_BASE + cmd,
	                      KERNEL_SOCKPTR(&mif6ctl), sizeof(mif6ctl));
#else
	ret = kernel_setsockopt(family->sock, SOL_IPV6, MRT6_BASE + cmd,
	                        (char *)&mif6ctl, sizeof(mif6ctl));
#endif
	if (ret) {
		ERRMSG("setsockopt(cmd=0x%x mifi=%d ifindex=%d) failed: %d",
		       MRT6_BASE + cmd, ctl->vif, ctl->ifindex, ret);
	}
	return 0;
}

static int mcastd_mrt6_mfcctl(struct mcastd_mrt_family *family,
                              struct mcastd_mrt_mfcctl *ctl, int cmd)
{
	struct mf6cctl mf6cctl;
	int ret;
	int i;
	if (!family->sock) //sock already closed -> give up
		return -1;
	memset(&mf6cctl, 0, sizeof(mf6cctl));
	mf6cctl.mf6cc_parent = ctl->iif;
	memcpy(&mf6cctl.mf6cc_origin.sin6_addr, ctl->src.data32, 16);
	memcpy(&mf6cctl.mf6cc_mcastgrp.sin6_addr, ctl->grp.data32, 16);
	DEBMSG("setsockopt(cmd=0x%x iif=%d grp=%s src=%s", MRT6_BASE + cmd,
	       ctl->iif, mdb_address_to_string(&ctl->grp),
	       mdb_address_to_string(&ctl->src));
	for (i = 0; i < MAXVIFS; i++) {
		if (!ctl->oif[i])
			continue;
		IF_SET(i, &mf6cctl.mf6cc_ifset);
		DEBMSG("           oif=%d", i);
	}
	DEBMSG("           )");
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ret = ipv6_setsockopt(family->sock->sk, SOL_IPV6, MRT6_BASE + cmd,
	                      KERNEL_SOCKPTR(&mf6cctl), sizeof(mf6cctl));
#else
	ret = kernel_setsockopt(family->sock, SOL_IPV6, MRT6_BASE + cmd,
	                        (char *)&mf6cctl, sizeof(mf6cctl));
#endif
	if (ret) {
		ERRMSG("setsockopt(cmd=0x%x iif=%d grp=%s src=%s) failed: %d",
		       MRT6_BASE + cmd, ctl->iif,
		       mdb_address_to_string(&ctl->grp),
		       mdb_address_to_string(&ctl->src), ret);
	}
	return ret;

}

static int mcastd_mrt6_recv(struct mcastd_mrt_family *family,
                            struct mcastd_mrt_msg *msg)
{
	int ret = -1;
	struct msghdr msghdr;
	struct kvec vec;
	struct mrt6msg mrt6msg;
	memset(&msghdr, 0, sizeof(msghdr));
	vec.iov_base = &mrt6msg;
	vec.iov_len = sizeof(mrt6msg);
	if (!family->sock) {
		DEBMSG("mrt6 sock is alread closed -> don't try to read"
		       " from it");
		return -1;
	}
	ret = kernel_recvmsg(family->sock, &msghdr, &vec, 1, vec.iov_len,
	                     MSG_DONTWAIT);
	if (ret < 0) {
		DEBMSG("kernel_recvmsg() returned error: %d", ret);
		return -1;
	} else if (!ret) {
		DEBMSG("Checked mrt6 sock, but no message available");
		return -1;
	} else if (ret < sizeof(struct mrt6msg)) {
		DEBMSG("Ignoring short message (ret=%d)", ret);
		return -1;
	}
	if (mrt6msg.im6_msgtype != MRT6MSG_NOCACHE) {
		DEBMSG("Ignoring mrt6msg with msgtype=%d", mrt6msg.im6_msgtype);
		return -1;
	}
	if (!mrt6msg.im6_mif || mrt6msg.im6_mif > MAXVIFS) {
		DEBMSG("Ignoring mrt6msg with mif=%d", mrt6msg.im6_mif);
		return -1;
	}
	msg->iif = mrt6msg.im6_mif;
	msg->src.family = msg->dst.family = AF_INET6;
	memcpy(msg->src.data, &mrt6msg.im6_src, 16);
	memcpy(msg->dst.data, &mrt6msg.im6_dst, 16);
	if (msg->dst.data[1] <= 0x02) {
		DEBMSG("Ignoring mrt6msg with link-local dst %s",
				mdb_address_to_string(&msg->dst));
		return -1;
	}
	return 0;
}

static void mcastd_mrt_mfc_expire(struct mcastd_mrt_mfc *mfc)
{
	mfc->family->mfcctl(mfc->family, &mfc->ctl, MCASTD_MRT_CMD_DEL_MFC);
	list_del(&mfc->head);
	kfree(mfc);
}

static void mcastd_mrt_mfc_timer_handler(void *data)
{
	mcastd_mrt_mfc_expire(data);
}

static void mcastd_mrt_mfc_schedule(struct mcastd_mrt_mfc *mfc)
{
	struct mdb_iface *irec;
	struct mcastd_mrt_iface *iaux;
	irec = mfc->family->vifs[mfc->ctl.iif];
	iaux = irec ? mdb_iface_auxdata(irec, MDB_REF_MRT) : NULL;
	mcastd_timer_schedule(&mfc->timer, iaux?iaux->deldelay:0);
}

static void mcastd_mrt_mfc_set(struct mcastd_mrt_family *family,
                               struct mcastd_mrt_mfcctl *ctl,
                               struct mcastd_mrt_mfc *mfc)
{
	int anyoif = 0;
	// Check if we have at least one oif. If so -> anyoif!=0
	for (anyoif = MAXVIFS - 1; anyoif > 0; anyoif--) {
		if (ctl->oif[anyoif])
			break;
	}
	// Find mfc entry in our own cache if not found by the caller already
	if (!mfc) list_for_each_entry(mfc, &family->mfc, head) {
		if (!mdb_address_equals(&ctl->grp, &mfc->ctl.grp))
			continue;
		if (!mdb_address_equals(&ctl->src, &mfc->ctl.src))
			continue;
		break;
	}
	// If not found -> mfc = NULL
	if (&mfc->head == &family->mfc)
		mfc = NULL;
	else
		mcastd_timer_cancel(&mfc->timer);

	if ((!anyoif || !ctl->iif) && !mfc) {
		// Had no oif/iif and still no oif/iif -> nop!
		return;
	} else if (!mfc) {
		// Have oif/iif but no cache entry found -> create!
		mfc = kzalloc(sizeof(struct mcastd_mrt_mfc), GFP_KERNEL);
		if (!mfc) {
			ERRMSG("kzalloc() failed");
			return;
		}
		mfc->family = family;
		mcastd_timer_init(&mfc->timer, mcastd_mrt_mfc_timer_handler, NULL,
		                mfc, &mdb_mutex);
		list_add_tail(&mfc->head, &family->mfc);
	} else if (!anyoif || !ctl->iif) {
		// Had oif/iif but now not anymore -> mark for clean up
		mcastd_mrt_mfc_schedule(mfc);
		return;
	} else if (!memcmp(&mfc->ctl, ctl, sizeof(struct mcastd_mrt_mfcctl))) {
		// Had oif and still have oif, nothing changed -> nop!
		return;
	} // else {} // Had oif and still have oif, but different -> update!
	mfc->family->mfcctl(mfc->family, ctl, MCASTD_MRT_CMD_ADD_MFC);
	memcpy(&mfc->ctl, ctl, sizeof(struct mcastd_mrt_mfcctl));
}

static void mcastd_mrt_nocache_handler(struct mcastd_mrt_family *family,
                                       struct mcastd_mrt_msg *msg)
{
	struct mdb_iface *irec;
	struct mcastd_mrt_iface *iaux;
	struct mdb_group *grec;
	struct mdb_source *srec;
	struct mcastd_mrt_mfcctl ctl;
	DEBMSG("Handle nocache iif=%d src=%s dst=%s", msg->iif,
		mdb_address_to_string(&msg->src),
		mdb_address_to_string(&msg->dst));
	if (msg->iif >= MAXVIFS) {
		DEBMSG("Invalid vif %d >= MAXVIFS %d", msg->iif, MAXVIFS);
		return;
	}
	irec = family->vifs[msg->iif];
	if (!irec || !(irec->refs & 1 << MDB_REF_MRT)) {
		DEBMSG("No such iface - vif=%d", msg->iif);
		return;
	}
	iaux = mdb_iface_auxdata(irec, MDB_REF_MRT);
	if (!iaux || !iaux->iifenable) {
		DEBMSG("Iface %d not enabled as iif", msg->iif);
		return;
	}
	grec = mdb_group_mfind(&msg->dst);
	if (!grec) {
		DEBMSG("No group record found -> leave early");
		return;
	}
	memset(&ctl, 0, sizeof(ctl));
	ctl.iif = msg->iif;
	memcpy(&ctl.src, &msg->src, sizeof(struct mdb_address));
	memcpy(&ctl.grp, &msg->dst, sizeof(struct mdb_address));
	do {
		if (!(grec->refs & MDB_REFMASK_SNOOPER))
			goto next_grec;
		if (!(grec->iface->refs & 1 << MDB_REF_MRT))
			goto next_grec;
		iaux = mdb_iface_auxdata(grec->iface, MDB_REF_MRT);
		if (!iaux->oifenable)
			goto next_grec;
		srec = mdb_source_find(grec, &msg->src);
		if (srec && !(srec->refs & MDB_REFMASK_SNOOPER))
			srec = NULL;
		if (srec ?
				!(srec->refs & MDB_REFMASK_SNOOPER_IN) :
				!(grec->refs & MDB_REFMASK_SNOOPER_EX))
			goto next_grec;
		DEBMSG("    set oif %d", iaux->vif);
		ctl.oif[iaux->vif] = 1;
next_grec:
		grec = list_entry(grec->mdups.next, struct mdb_group, mdups);
	} while (grec->mlead != grec);
	mcastd_mrt_mfc_set(family, &ctl, NULL);
}

static void mcastd_mrt_sock_handler(void *data) {
	struct mcastd_mrt_msg msg;
	struct mcastd_mrt_family *family = data;
	mdb_lock();
	if (family->recv(family, &msg))
		goto leave;
	mcastd_mrt_nocache_handler(family, &msg);
leave:
	mdb_unlock();
}

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0))
static int mcastd_mrt_wakefunc(wait_queue_entry_t *wait, unsigned mode, int flags,
#else
static int mcastd_mrt_wakefunc(wait_queue_t *wait, unsigned mode, int flags,
#endif
                             void *key)
{
	struct mcastd_work work = {
		.run = mcastd_mrt_sock_handler,
		.destroy = NULL,
		.data = wait->private
	};
	mcastd_work_schedule(&work, GFP_ATOMIC);
	return 0;
}

static struct mcastd_mrt_family mrt_family[2] = {
	{
		.mfc = LIST_HEAD_INIT(mrt_family[0].mfc),
		.vifs = { [0] = NULL },
		.sock = NULL,
		.wait = {
			.flags     = 0,
			.private   = &mrt_family[0],
			.func      = mcastd_mrt_wakefunc,
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0))
			.entry = LIST_HEAD_INIT(mrt_family[0].wait.entry)
#else
			.task_list = LIST_HEAD_INIT(mrt_family[0].wait.task_list)
#endif
		},
		.init = mcastd_mrt4_init,
		.done = mcastd_mrt4_done,
		.vifctl = mcastd_mrt4_vifctl,
		.mfcctl = mcastd_mrt4_mfcctl,
		.recv = mcastd_mrt4_recv,
	}, {
		.mfc = LIST_HEAD_INIT(mrt_family[1].mfc),
		.vifs = { [0] = NULL },
		.sock = NULL,
		.wait = {
			.flags     = 0,
			.private   = &mrt_family[1],
			.func      = mcastd_mrt_wakefunc,
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0))
			.entry = LIST_HEAD_INIT(mrt_family[0].wait.entry)
#else
			.task_list = LIST_HEAD_INIT(mrt_family[0].wait.task_list)
#endif
		},
		.init = mcastd_mrt6_init,
		.done = mcastd_mrt6_done,
		.vifctl = mcastd_mrt6_vifctl,
		.mfcctl = mcastd_mrt6_mfcctl,
		.recv = mcastd_mrt6_recv,
	}
};

static struct mcastd_mrt_family *mcastd_mrt_getfamily(int family) {
	switch (family) {
	case AF_INET:  return &mrt_family[0];
	case AF_INET6: return &mrt_family[1];
	default:
		return NULL;
	}
}

static void mcastd_mrt_mdb_handler(struct mdb_event *e)
{
	struct mcastd_mrt_iface *iaux;
	struct mcastd_mrt_mfc *mfc, *mfcnxt;
	struct mcastd_mrt_mfcctl ctl;
	struct mdb_source *srec;
	struct mcastd_mrt_family *family =
			mcastd_mrt_getfamily(e->group->address.family);
	if (!family)
		return;
	if (!(e->change[MDB_LEVEL_GROUP] & MDB_REFMASK_SNOOPER) &&
			!(e->change[MDB_LEVEL_SOURCE] & MDB_REFMASK_SNOOPER))
		return;
	if (!(e->group->iface->refs & 1 << MDB_REF_MRT))
		return;
	iaux = mdb_iface_auxdata(e->group->iface, MDB_REF_MRT);
	if (!iaux->oifenable)
		return;
	DEBMSG("Handle mdb event ifindex=%d vif=%d group=%s refs=0x%x",
	       e->group->iface->ifindex, iaux->vif,
	       mdb_address_to_string(&e->group->address), e->group->refs);
	list_for_each_entry_safe(mfc, mfcnxt, &family->mfc, head) {
		if (!mdb_address_equals(&mfc->ctl.grp, &e->group->address))
			continue;
		memcpy(&ctl, &mfc->ctl, sizeof(struct mcastd_mrt_mfcctl));
		srec = mdb_source_find(e->group, &ctl.src);
		if (srec && !(srec->refs & MDB_REFMASK_SNOOPER))
			srec = NULL;
		if (srec ?
				(srec->refs     & MDB_REFMASK_SNOOPER_IN) :
				(e->group->refs & MDB_REFMASK_SNOOPER_EX))
			ctl.oif[iaux->vif] = 1;
		else
			ctl.oif[iaux->vif] = 0;
		DEBMSG("    %s oif %d for src %s",
			ctl.oif[iaux->vif] ? "set" : "clear", iaux->vif,
			mdb_address_to_string(&ctl.src));
		mcastd_mrt_mfc_set(family, &ctl, mfc);
	}
}

static int mcastd_mrt_vifselect(struct mcastd_mrt_iface *iaux, int hint) {
	struct mcastd_mrt_family *family = mcastd_mrt_getfamily(iaux->addr.family);
	int i;
	if (!family)
		return 0;
	if (!iaux->iifenable && !iaux->oifenable)
		return 0;
	if (hint)
		return hint;
	for (i = 1; i < MAXVIFS; i++)
		if (!family->vifs[i])
			return i;
	return 0;
}

static void mcastd_mrt_intf_set(struct mdb_iface *irec,
                                struct mcastd_mrt_iface *config)
{
	struct mcastd_mrt_iface *iaux;
	struct mcastd_mrt_family *family;
	struct mcastd_mrt_mfc *mfc, *mfcnext;
	struct mcastd_mrt_mfcctl mfcctl;
	struct mcastd_mrt_vifctl vifctl;
	iaux = mdb_iface_auxdata(irec, MDB_REF_MRT);
	if (!iaux)
		return;
	config->vif = mcastd_mrt_vifselect(config, iaux->vif);
	if (!memcmp(config, iaux, sizeof(struct mcastd_mrt_iface)))
		return;
	if (iaux->vif) { // break down
		family = mcastd_mrt_getfamily(iaux->addr.family);
		DEBMSG("delvif ifindex=%d vif=%d", iaux->ifindex, iaux->vif);
		family->vifs[iaux->vif] = NULL;
		list_for_each_entry_safe(mfc, mfcnext, &family->mfc, head) {
			memcpy(&mfcctl, &mfc->ctl, sizeof(mfcctl));
			if (mfcctl.iif == iaux->vif) {
				DEBMSG("Clear iif %d for %s->%s", iaux->vif,
					mdb_address_to_string(&mfcctl.src),
					mdb_address_to_string(&mfcctl.grp));
				mfcctl.iif = 0;
			}
			if (mfcctl.oif[iaux->vif]) {
				DEBMSG("Clear oif %d for %s->%s", iaux->vif,
					mdb_address_to_string(&mfcctl.src),
					mdb_address_to_string(&mfcctl.grp));
				mfcctl.oif[iaux->vif] = 0;
			}
			mcastd_mrt_mfc_set(family, &mfcctl, mfc);
		}
		vifctl.vif = iaux->vif;
		vifctl.ifindex = iaux->ifindex;
		memcpy(&vifctl.addr, &iaux->addr, sizeof(vifctl.addr));
		family->vifctl(family, &vifctl, MCASTD_MRT_CMD_DEL_VIF);
		irec->refs &= ~(1 << MDB_REF_MRT);
	}
	if (config->vif) { // set up
		family = mcastd_mrt_getfamily(config->addr.family);
		irec->refs |= 1 << MDB_REF_MRT;
		DEBMSG("addvif ifindex=%d vif=%d addr=%s",
		       irec->ifindex, config->vif,
		       mdb_address_to_string(&config->addr));
		vifctl.vif = config->vif;
		vifctl.ifindex = config->ifindex;
		memcpy(&vifctl.addr, &config->addr, sizeof(vifctl.addr));
		family->vifctl(family, &vifctl, MCASTD_MRT_CMD_ADD_VIF);
		family->vifs[config->vif] = irec;
		memcpy(iaux, config, sizeof(struct mcastd_mrt_iface));
	}
	memcpy(iaux, config, sizeof(struct mcastd_mrt_iface));
	mdb_iface_release(irec);
}

static int mcastd_mrt_intf_handler(struct nlmsghdr *nlh)
{
	struct mdb_iface *irec;
	struct mcastd_mrt_iface config;
	struct kvar *v0, *v1, *v2;
	const char *key;
	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	irec = mdb_iface_find(key);
	if (!irec)
		goto leave;
	memset(&config, 0, sizeof(struct mcastd_mrt_iface));
	switch (nlh->nlmsg_type) {
	case MCASTDNL_TYPE_NEWOBJ:
		v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
		v2 = kvar_map_find(v1, "MRTIIFEnable");
		config.iifenable = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "MRTOIFEnable");
		config.oifenable = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "MRTIIFDeleteDelay");
		config.deldelay = kvar_uint_val(v2);
		v2 = kvar_map_find(v1, "Address");
		if (v2 && kvar_addr_val(v2)) {
			config.addr.family = kvar_addr_fam(v2);
			memcpy(config.addr.data, kvar_addr_val(v2),
			       mdb_address_size(&config.addr));
		}
		v2 = kvar_map_find(v1, "NetDevIndex");
		config.ifindex = kvar_int_val(v2);
		break;
	case MCASTDNL_TYPE_DELOBJ:
		break;
	default:
		goto leave;
	}
	mcastd_mrt_intf_set(irec, &config);
leave:
	mdb_unlock();
	return 0;
}

static int mcastd_mrt_iface2var(struct kvarbuf *buf, struct kvar *var, void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastd_mrt_iface *iaux = data;
	kvar_map_init(buf, v0, 4);
	kvar_map_edit(buf, v0, 0, "iifenable", &v1);
	kvar_uint_set(buf, v1, iaux->iifenable);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "oifenable", &v1);
	kvar_uint_set(buf, v1, iaux->oifenable);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "vif", &v1);
	kvar_uint_set(buf, v1, iaux->vif);
	kvar_map_done(buf, v0, 2);
	kvar_map_edit(buf, v0, 3, "deldelay", &v1);
	kvar_uint_set(buf, v1, iaux->deldelay);
	kvar_map_done(buf, v0, 3);
	return 0;
}

static int mcastd_mrt_feed_handler(struct nlmsghdr *nlh)
{
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct mcastd_mrt_msg msg;
	struct sk_buff *skb;
	int ret = -EINVAL;
	memset(&msg, 0, sizeof(msg));
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
	v2 = kvar_map_find(v1, "iif");
	if (!kvar_int_val(v2) || kvar_int_val(v2) > MAXVIFS) {
		ERRMSG("iif missing or invalid");
		goto leave;
	}
	msg.iif = kvar_int_val(v2);
	v2 = kvar_map_find(v1, "src");
	if (!kvar_addr_val(v2)) {
		ERRMSG("src address missing or invalid");
		goto leave;
	}
	msg.src.family = kvar_addr_fam(v2);
	memcpy(msg.src.data, kvar_addr_val(v2), mdb_address_size(&msg.src));

	v2 = kvar_map_find(v1, "dst");
	if (!kvar_addr_val(v2)) {
		ERRMSG("dst address missing or invalid");
		goto leave;
	}
	msg.dst.family = kvar_addr_fam(v2);
	memcpy(msg.dst.data, kvar_addr_val(v2), mdb_address_size(&msg.dst));

	mdb_lock();
	mcastd_mrt_nocache_handler(mcastd_mrt_getfamily(msg.src.family), &msg);
	mdb_unlock();

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	return ret;
}

static struct mdb_auxinfo mcastd_mrt_iface_auxinfo = {
	.level    = MDB_LEVEL_IFACE,
	.ref      = MDB_REF_MRT,
	.name     = "mrt",
	.size     = sizeof(struct mcastd_mrt_iface),
	.data2var = mcastd_mrt_iface2var
};

static void mcastd_mrt_cleanup(void)
{
	struct mcastd_mrt_iface config;
	struct mdb_iface *irec, *irecnxt;
	int i;
	DEBMSG("Unloading mcastd_mrt");
	memset(&config, 0, sizeof(struct mcastd_mrt_iface));
	mcastd_object_unregister("MCASTD.Intf.*", mcastd_mrt_intf_handler);
	mcastd_function_unregister("MCASTD", "mrtfeed", mcastd_mrt_feed_handler);
	mdb_lock();
	mdb_unsubscribe(MDB_PRIORITY_MED, mcastd_mrt_mdb_handler);
	list_for_each_entry_safe(irec, irecnxt, &mdb_ifaces, head)
		mcastd_mrt_intf_set(irec, &config);
	for (i = 0; i < ARRAY_SIZE(mrt_family); i++) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,3,0)
		remove_wait_queue(&mrt_family[i].sock->wq.wait,
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,35)
		remove_wait_queue(&mrt_family[i].sock->wq->wait,
#else
		remove_wait_queue(&mrt_family[i].sock->wait,
#endif
				&mrt_family[i].wait);
		mrt_family[i].done(&mrt_family[i]);
	}
	mdb_unregister_aux(&mcastd_mrt_iface_auxinfo);
	mdb_unlock();
}

static int mcastd_mrt_init(void)
{
	int i;
	DEBMSG("Loading mcastd_mrt");
	mdb_lock();
	mdb_register_aux(&mcastd_mrt_iface_auxinfo);
	for (i = 0; i < ARRAY_SIZE(mrt_family); i++) {
		if (mrt_family[i].init(&mrt_family[i]))
			return -1;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,3,0)
		add_wait_queue(&mrt_family[i].sock->wq.wait,
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,35)
		add_wait_queue(&mrt_family[i].sock->wq->wait,
#else
		add_wait_queue(&mrt_family[i].sock->wait,
#endif
				&mrt_family[i].wait);
	}
	mdb_subscribe(MDB_PRIORITY_MED, mcastd_mrt_mdb_handler);
	mdb_unlock();
	mcastd_object_register("MCASTD.Intf.*", mcastd_mrt_intf_handler);
	mcastd_function_register("MCASTD", "mrtfeed", mcastd_mrt_feed_handler);
	return 0;
}

module_init(mcastd_mrt_init);
module_exit(mcastd_mrt_cleanup);

